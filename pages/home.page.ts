import { BrowserContext, Locator, Page } from '@playwright/test';
import BasePage from './base.page';
import envParams from '../params';

export default class HomePage extends BasePage {
  readonly page: Page;
  readonly context: BrowserContext;
  readonly container: Locator;
  readonly title: Locator;

  constructor(page: Page, context: BrowserContext) {
    super(page, context);
    this.page = page;
    this.context = context;
    this.container = this.page.locator('.ssls-home-page');
    this.title = this.container.locator('h1.ssls-home-page-h1');
  }

  async open(): Promise<HomePage> {
    await this.page.goto(envParams.baseUrl);
    await this.waitToLoad();
    return this;
  }
  getTitle(): Promise<string> {
    return this.title.textContent();
  }

  async waitToLoad(): Promise<void> {
    await this.title.waitFor({ state: 'visible' });
  }
}
