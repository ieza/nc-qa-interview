import { test, expect } from '@playwright/test';
import HomePage from '../pages/home.page';
import homePageData from '../data/homePage.data';

test('Home page title', async ({ page, context }) => {
  const homePage = new HomePage(page, context);
  await homePage.open();

  await expect(await homePage.getTitle(), 'should be displayed').toEqual(homePageData.title);
});
