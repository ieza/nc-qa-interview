# NC QA Interview

## Prerequisites

1. You should have NodeJS v20 or higher installed
2. You should have Google Chrome browser installed

## Install dependencies

To install dependencies run `npm i`

## Run tests

To run all tests use `npm run test`

It is also possible to run specific spec `npm run test tests/homePage.spec.ts`

## Running tests in UI mode

To run tests in UI mode use `npm run test:ui`

## Working with the code

Use `npm run lint` to check for linter errors and `npm run lint:fix` to fix them, if possible
Use `npm run prettier` to check for code formatting errors and `npm run prettier:fix` to fix them, if possible
