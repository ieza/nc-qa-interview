import EnvParams from './types/EnvParams';

const envParams: EnvParams = {
  baseUrl: process.env.BASE_URL,
};

export default envParams;
