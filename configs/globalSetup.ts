import * as dotenv from 'dotenv';

async function globalSetup() {
  if (process.env.CI !== 'true') {
    console.log('Running tests locally.........');
    dotenv.config();
  }
}

export default globalSetup;
